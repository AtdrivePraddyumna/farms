<?php
App::uses('Controller', 'Controller');
App::uses('Security', 'Utility'); 
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'smtp', array('file' => 'phpmailer'.DS.'class.smtp.php'));
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer'.DS.'class.phpmailer.php'));
class WebServicesController extends AppController 
{
	var $base_path = WWW_ROOT;

	public function beforeFilter()
	{
		parent::beforeFilter();	
		$this->writeLog();
		$this->base_url = "http://".$_SERVER['SERVER_NAME'].Router::url('/');
		$this->autoRender = false;
	}

	public function registration()
	{
		$data = file_get_contents("php://input");	
	    $decoded = json_decode($data, true);
	    $this->loadModel("Reporter"); 
	    $url = Router::url('/',true);
		$upload_path = 'img/'; 
		//$url = Router::url('/',true);
	    //$url = "http://localhost/yoruba/";
	 //    $url = "http://in.atdrive.com:835/farms/";
		// $upload_path = 'assets/gallery/images/'; 
	    if(!empty($decoded))
	    {	    	
	    	if(isset($decoded['email_id']) && $decoded['email_id'] != '')
	    	{
	    		$userdata = $this->Reporter->find('first', array(
			        'conditions' => array('Reporter.email_id' => $decoded['email_id'])
			    ));
			    if(!empty($userdata))
			    {
			    	$this->response->statusCode(400);			
					$responseData['success'] = false;
					$responseData['message'] = "User Already Exists";  
					return json_encode($responseData); 
			    }
			    $decoded_data = array(
					'name'=>$decoded['name'],
					'mobile_no'=>$decoded['mobile_no'],
					'email_id'=>$decoded['email_id'],
					//'password'=>Security::hash($decoded['password'], null, true)					
					'password'=>$decoded['password']					
					);
		    	$this->Reporter->save($decoded_data);
		    	$lastOrderInsertId = $this->Reporter->getInsertID();
		    	$this->response->statusCode(200);						
				$responseData['success'] = true;
				$responseData['message'] = "Reporter registration Successfull";
				$responseData['data'] = array('id' => $lastOrderInsertId);
				return json_encode($responseData);
	    	}
		    $this->response->statusCode(400);			
			$responseData['success'] = false;
			$responseData['message'] = "Data Not valid";  
			return json_encode($responseData); 		    	
	    } 
	    $this->response->statusCode(400);			
		$responseData['success'] = false;
		$responseData['message'] = "Data Not valid";  
		return json_encode($responseData); 
	}

	public function login_api()
	{
		$data = file_get_contents("php://input");	
	    $decoded = json_decode($data, true);
	    $this->loadModel("Reporter");   
	    $url = Router::url('/',true);
		$upload_path = 'img/';
	    if(isset($decoded['email_id']) && isset($decoded['password']))
	    {
		    $userdata = $this->Reporter->find('first', array(
		        'conditions' => array('Reporter.email_id' => $decoded['email_id'])
		    ));
			if(empty($userdata))
		    {
		    	$responseData['message'] = "Ah! Email didn't match";
		    	$this->response->statusCode(400);
		    	$responseData['success'] = false;
		    	return json_encode($responseData);	    		  
		    }
		    // $userimage = '';
		    // if(isset($userdata['User']['image']))
		    // {
		    // 	$userimage = $url.$upload_path.$userdata['User']['image'];	
		    // }	
		    $decoded_password = $decoded['password'];	    
		    //$decoded_password = Security::hash($decoded['password'], null, true);
		    $compare = strcmp($decoded_password,$userdata['Reporter']['password']);
		    if($compare==0)
		    {
		    	$responseData['message'] = "Login Successful";
		    	$this->response->statusCode(200);
				$responseData['success'] = true;
				$responseData['data'] = array(
					        'user_id' => $userdata['Reporter']['id'],
		                   	//'image' => $userimage,
		                   	//'first_name' => $userdata['User']['first_name'],
		                   	'name' => $userdata['Reporter']['name'],
		                   	'mobile_no' => $userdata['Reporter']['mobile_no'],
		                   	'email_id' => $userdata['Reporter']['email_id']
		               );
				$responseData = array_map(function($v){
			        return (is_null($v)) ? "" : $v;
			    },$responseData);
		        return json_encode($responseData);					
	    	}
	    	else
	    	{
	    		$this->response->statusCode(400);
	    		$responseData['message'] = "Ah! Email and Password didn't match";		    	
		    	$responseData['success'] = false;
		    	return json_encode($responseData);	
	    	}	    	
		}
	    else
	    {
	    	$this->response->statusCode(400);
	    	$responseData['message'] = "Login Failed";
	    	$responseData['success'] = false;
	    	return json_encode($responseData);	
	    }	    
	}

	public function save_farmer_report()
	{
		$data = file_get_contents("php://input");	
	    $decoded = json_decode($data, true);
	    $this->loadModel("Farmer"); 
	    $this->loadModel("Farmer_Crop");
	    $this->loadModel("Report"); 
	    $url = Router::url('/',true);
		$upload_path = 'img/';  
		//$decoded = $_POST;
	    if(!empty($decoded))
	    {
	  //   	if(isset($_FILES['crop_picture'] ) && count($_FILES['crop_picture']) > 0) 
		 //    {
			// 	$num_files = count($_FILES['crop_picture']);
			// }
		 //    else
		 //    	$num_files = 0;
		 //    if($num_files>0)
			// {
			// 	$filename = $_FILES['crop_picture']['name'];	
			// 	$url = Router::url('/',true);
			// 	$upload_path = 'img/';				
	  //   		//$url = "http://localhost/yoruba/";
			//     //$url = "http://in.atdrive.com:835/farms/";
			// 	// $upload_path = 'assets/gallery/images/';	
			// 	$upload_file = $upload_path.$filename;
			// 	if(move_uploaded_file($_FILES['crop_picture']['tmp_name'], $upload_file))
			// 	{
			// 		$decoded['crop_picture'] = $filename;				
			// 	}
			// 	else
			// 	{
			// 		$decoded['crop_picture'] = '';	
			// 	}
			// }
			$farmerdata = $this->Farmer->find('first', array(
		        'conditions' => array('Farmer.mobile_no' => $decoded['mobile_no'])
		    ));
		    if(!empty($farmerdata))
		    {
		    	$decoded_data = array(
	    			'diagnosis'=>isset($decoded['diagnosis']) ? $decoded['diagnosis'] : '',
					'solution'=>isset($decoded['solution']) ? $decoded['solution'] : '',
					'summary'=>isset($decoded['summary']) ? $decoded['summary'] : '',
					'date'=>isset($decoded['date']) ? $decoded['date'] : '',
					'next_follow_date'=>isset($decoded['next_follow_date']) ? $decoded['next_follow_date'] : '',
					'farmer_id' => isset($farmerdata['Farmer']['id']) ? $farmerdata['Farmer']['id'] : '',
					'reporter_id' => isset($decoded['reporter_id']) ? $decoded['reporter_id'] : ''
	    			);
		    	$this->Report->saveAssociated($decoded_data);
		    	$this->response->statusCode(200);						
				$responseData['success'] = true;
				$responseData['message'] = "Report Saved Successfully";
				return json_encode($responseData);
		    }
	    	$decoded_data = array(
	    		'Farmer' => array(
					'farmer_name'=>isset($decoded['farmer_name']) ? $decoded['farmer_name'] : '',
					'mobile_no'=>isset($decoded['mobile_no']) ? $decoded['mobile_no'] : '',
					'farm_address'=>isset($decoded['farm_address']) ? $decoded['farm_address'] : '',
					'lattitude'=>isset($decoded['lattitude']) ? $decoded['lattitude'] : '',
					'longitude'=>isset($decoded['longitude']) ? $decoded['longitude'] : ''
					),
	    		'Farmer_Crop' => array(
	    			'crop'=>isset($decoded['crop']) ? $decoded['crop'] : '',
					'crop_picture'=> isset($decoded['crop_picture']) ? $decoded['crop_picture'] : ''
	    			),
	    		'Report' => array(
	    			'diagnosis'=>isset($decoded['diagnosis']) ? $decoded['diagnosis'] : '',
					'solution'=>isset($decoded['solution']) ? $decoded['solution'] : '',
					'summary'=>isset($decoded['summary']) ? $decoded['summary'] : '',
					'date'=>isset($decoded['date']) ? $decoded['date'] : '',
					'reporter_id' => isset($decoded['reporter_id']) ? $decoded['reporter_id'] : ''
	    			)
	    		);
	    	$this->Farmer->saveAssociated($decoded_data, array('deep' => true));
	    	$lastReportInsertId = $this->Report->getInsertID();
	    	$this->response->statusCode(200);						
			$responseData['success'] = true;
			$responseData['message'] = "Farmer & Report Saved Successfully";
			$responseData['data'] = array('id' => $lastReportInsertId);
			return json_encode($responseData);
	    } 
	    $this->response->statusCode(400);			
		$responseData['success'] = false;
		$responseData['message'] = "Data Not valid";  
		return json_encode($responseData); 
	}

	public function save_farmer_crop()
	{
		$data = file_get_contents("php://input");	
	    $decoded = json_decode($data, true);
	    $this->loadModel("Farmer"); 
	    $this->loadModel("Farmer_Crop");
	    $this->loadModel("Report"); 
	    $url = Router::url('/',true);
		$upload_path = 'img/';  
		$decoded = $_POST;
	    if(!empty($_FILES))
	    {
	    	if(isset($_FILES['crop_picture'] ) && count($_FILES['crop_picture']) > 0) 
		    {
				$num_files = count($_FILES['crop_picture']);
			}
		    else
		    	$num_files = 0;
		    if($num_files>0)
			{
				$filename = $_FILES['crop_picture']['name'];	
				$url = Router::url('/',true);
				$upload_path = 'img/';				
	    		//$url = "http://localhost/yoruba/";
			 //    $url = "http://in.atdrive.com:835/farms/";
				// $upload_path = 'assets/gallery/images/';	
				$upload_file = $upload_path.$filename;
				if(move_uploaded_file($_FILES['crop_picture']['tmp_name'], $upload_file))
				{
					$decoded['crop_picture'] = $filename;				
				}
				else
				{
					$decoded['crop_picture'] = '';	
				}
			}
			// $decoded_data = array(
			// 	'crop' => isset($decoded['crop']) ? $decoded['crop'] : '',
			// 	 'crop_picture' => isset($decoded['crop_picture']) ? $decoded['crop_picture'] : '',
			// 	 'farmer_id' => isset($decoded['farmer_id']) ? $decoded['farmer_id'] : ''
			// 	 );
			// $this->Farmer_Crop->save($decoded_data);
			// $lastReportInsertId = $this->Farmer_Crop->getInsertID();
	  //   	$this->response->statusCode(200);						
			// $responseData['success'] = true;
			// $responseData['message'] = "Farmer Crop Saved Successfully";
			// $responseData['data'] = array('id' => $lastReportInsertId);
			// return json_encode($responseData);

	    	$this->response->statusCode(200);						
			$responseData['success'] = true;
			$responseData['message'] = "Crop Picture Saved Successfully";
			$responseData['data'] = array('crop_picture' => $decoded['crop_picture']);
			return json_encode($responseData);
	    }
	    $this->response->statusCode(400);			
		$responseData['success'] = false;
		$responseData['message'] = "Data Not valid";  
		return json_encode($responseData); 
	}

	public function followed_report()
	{
		$data = file_get_contents("php://input");	
	    $decoded = json_decode($data, true);
	    $this->loadModel("Farmer"); 
	    $this->loadModel("Farmer_Crop");
	    $this->loadModel("Report"); 	    
	    $this->loadModel("Followed_Report"); 	    
	    if(!empty($decoded))
	    {	  			
			$check = $this->Followed_Report->save($decoded);      
			if($check)
			{
				$this->response->statusCode(200);					  	
				$responseData['success'] = true;
				$responseData['message'] = "Followed-up Report Saved Successfully";
				return json_encode($responseData);	
			}
    	  	$this->response->statusCode(400);			
			$responseData['success'] = false;
			$responseData['message'] = "Followed-up Report Not Saved";  
			return json_encode($responseData); 

	    } 
	    $this->response->statusCode(400);			
		$responseData['success'] = false;
		$responseData['message'] = "Data Not valid";  
		return json_encode($responseData); 
	}

	public function get_my_reports()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
	    $upload_path = 'img/';				
		//$url = "http://in.atdrive.com:835/farms/";
		//$upload_path = 'assets/gallery/images/';	
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    if(isset($decoded['reporter_id']))
	    {	    	
	    	$count = $this->Report->find('count', array(	    				
				'conditions' => array(
					'Report.id' => $decoded['reporter_id']
        		),                   				
        	));
	    	if($offset > 0 )
	    	{
	    		$offset = $limit * $offset;
	    	}
	    	if(isset($decoded['sortByFollowUpDate']) && $decoded['sortByFollowUpDate'] == true)
	    	{	    		
	    		if(isset($decoded['date']) && $decoded['date'] !='')
	    		{
	    			$my_reports = $this->Report->find('all', array(		
					'conditions' => array(
						'Report.reporter_id' => $decoded['reporter_id'],
						'Report.next_follow_date' =>$decoded['date']
	        		),  
	        		'recursive' => 2,		                 
					'limit' => $limit,
					'offset' => $offset,
					'order'=> 'date DESC'
		        	));	
	    		}
	    		else
	    		{
	    			$my_reports = $this->Report->find('all', array(		
					'conditions' => array(
						'Report.reporter_id' => $decoded['reporter_id']
	        		),  
	        		'recursive' => 2,		                 
					'limit' => $limit,
					'offset' => $offset,
					'order'=> 'date DESC'
		        	));	
	    		}
	    		
	    	}
	    	else
	    	{
	    		if(isset($decoded['date']) && $decoded['date'] !='')
	    		{
		    		$my_reports = $this->Report->find('all', array(		
					'conditions' => array(
						'Report.reporter_id' => $decoded['reporter_id'],
						'Report.next_follow_date' =>$decoded['date']
	        		),  
	        		'recursive' => 2,		                 
					'limit' => $limit,
					'offset' => $offset
		        	));
	        	}
	        	else
	        	{
	        		$my_reports = $this->Report->find('all', array(		
					'conditions' => array(
						'Report.reporter_id' => $decoded['reporter_id']
	        		),  
	        		'recursive' => 2,		                 
					'limit' => $limit,
					'offset' => $offset
		        	));
	        	}	
	    	}
	    	if(!empty($my_reports))
	    	{
	    		foreach ($my_reports as $key => &$value) {
	    			unset($value['Farmer']['Report']);	    	    			
	    			if(isset($value['Farmer']['Farmer_Crop']['crop_picture']) && $value['Farmer']['Farmer_Crop']['crop_picture'] != '')			
	    			{
	    				$value['Farmer']['Farmer_Crop']['crop_picture'] = $url.$upload_path.$value['Farmer']['Farmer_Crop']['crop_picture'];
	    			}
	    		}
	    	}
	    	//$my_reports = Set::classicExtract($my_reports, '{n}.Report');
	    	if($my_reports)
	    	{
	    		$this->response->statusCode(200);
		    	$responseData['message'] = "Report list Successfully fetched";
		    	$responseData['success'] = true;
		    	$responseData['data'] = $my_reports;
		    	$responseData['offset'] = $offset;
		    	$responseData['count'] = $count;
		    	$responseData = array_map(function($v){
			        return (is_null($v)) ? "" : $v;
			    },$responseData);
				return json_encode($responseData);
			}
			else
			{
				$this->response->statusCode(400);
				$responseData['success'] = false;
				$responseData['message'] = "Could not fetch Report data";	
				return json_encode($responseData);
			}
	    } 
	    else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Report data";	
			return json_encode($responseData);
		}  				    
	}

	public function get_farmer_reports()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
		$upload_path = 'img/';				
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    if(isset($decoded['farmer_id']))
	    {	    	
	    	$count = $this->Report->find('count', array(	    				
				'conditions' => array(
					'Report.farmer_id' => $decoded['farmer_id']
        		),                   				
        	));
	    	if($offset > 0 )
	    	{
	    		$offset = $limit * $offset;
	    	}
	    	    		
	    	$my_reports = $this->Report->find('all', array(
	    		//'fields' => array('Report.*'),			
				'conditions' => array(
					'Report.farmer_id' => $decoded['farmer_id']
        		),                   
				'limit' => $limit,
				'offset' => $offset
	        	));
	    	//$my_reports = Set::classicExtract($my_reports, '{n}.Report');
	    	if($my_reports)
	    	{
	    		$this->response->statusCode(200);
		    	$responseData['message'] = "Report list Successfully fetched";
		    	$responseData['success'] = true;
		    	$responseData['data'] = $my_reports;
		    	$responseData['offset'] = $offset;
		    	$responseData['count'] = $count;
		    	$responseData = array_map(function($v){
			        return (is_null($v)) ? "" : $v;
			    },$responseData);
				return json_encode($responseData);
			}
			else
			{
				$this->response->statusCode(400);
				$responseData['success'] = false;
				$responseData['message'] = "Could not fetch Report data";	
				return json_encode($responseData);
			}
	    } 
	    else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Report data";	
			return json_encode($responseData);
		}  				    
	}

	public function get_all_reports()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);	
	    $upload_path = 'img/';					
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    	    	
    	$count = $this->Report->find('count');
    	if($offset > 0 )
    	{
    		$offset = $limit * $offset;
    	}
    	    		
    	$my_reports = $this->Report->find('all', array(
    		//'fields' => array('Report.*'),						          
			'limit' => $limit,
			'offset' => $offset,
			'recursive' => 1
        	));
    	//$my_reports = Set::classicExtract($my_reports, '{n}.Report');
    	if($my_reports)
    	{    		
    		foreach ($my_reports as $key => &$value) 
    		{    	   
				if(isset($value['Farmer']['Farmer_Crop']) && !empty($value['Farmer']['Farmer_Crop']))
				{
					$value['Farmer']['Farmer_Crop']['crop_picture'] = $url.$upload_path.$value['Farmer']['Farmer_Crop']['crop_picture'];	    				
				} 						
    		}
    		$this->response->statusCode(200);
	    	$responseData['message'] = "Report list Successfully fetched";
	    	$responseData['success'] = true;
	    	$responseData['data'] = $my_reports;
	    	$responseData['offset'] = $offset;
	    	$responseData['count'] = $count;
	    	$responseData = array_map(function($v){
		        return (is_null($v)) ? "" : $v;
		    },$responseData);
			return json_encode($responseData);
		}
		else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Report data";	
			return json_encode($responseData);
		}	    		   
	}

	public function get_all_products()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
	    $upload_path = 'img/';				
	    //$url = "http://localhost/yoruba/";
	 //    $url = "http://in.atdrive.com:835/farms/";
		// $upload_path = 'assets/gallery/images/';	
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $this->loadModel("Product");	
	    $this->loadModel("Product_Speciality");	
	    $this->loadModel("Product_Benefits");	
	    $this->loadModel("Product_Category");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    	    	
    	$count = $this->Product_Category->find('count');
    	if($offset > 0 )
    	{
    		$offset = $limit * $offset;
    	}
    	    		
    	$my_products = $this->Product_Category->find('all', array(
    		//'fields' => array('Product.*'),	
    		'recursive' => 2,					          
			'limit' => $limit,
			'offset' => $offset
        	));
    	//$my_products = Set::classicExtract($my_products, '{n}.Product');    
    	if($my_products)
    	{ 
    		foreach ($my_products as $key => &$value) 
    		{
				foreach ($value['Product'] as $k => &$val)
				{
					if(isset($val['product_picture']) && $val['product_picture'] !='')
					{
						$val['product_picture'] = $url.$upload_path.$val['product_picture'];
					}
				}    			
    		}
    		$this->response->statusCode(200);
	    	$responseData['message'] = "Product list Successfully fetched";
	    	$responseData['success'] = true;
	    	$responseData['data'] = $my_products;
	    	$responseData['offset'] = $offset;
	    	$responseData['count'] = $count;
	    	$responseData = array_map(function($v){
		        return (is_null($v)) ? "" : $v;
		    },$responseData);
			return json_encode($responseData);
		}
		else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Product data";	
			return json_encode($responseData);
		}	    		   
	}

	public function get_farmer_by_id()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
		$upload_path = 'img/';				
	    $this->loadModel("Farmer");	
	    $this->loadModel("Report");	 
	    
	    if(isset($decoded['farmer_id']))
	    {	    		    		    	    	
	    	$farmer = $this->Farmer->find('first', array(
	    		'fields' => array('Farmer.*'),			
				'conditions' => array(
					'Farmer.id' => $decoded['farmer_id']
        		)
	        	));

	    	//$farmer = Set::classicExtract($farmer, '{n}.Farmer');
	    	//echo "<pre>";print_r($farmer);exit;
	    	if($farmer)
	    	{
	    		$this->response->statusCode(200);
		    	$responseData['message'] = "Farmer details Successfully fetched";
		    	$responseData['success'] = true;
		    	$responseData['data'] = $farmer;
		    	$responseData = array_map(function($v){
			        return (is_null($v)) ? "" : $v;
			    },$responseData);
				return json_encode($responseData);
			}
			else
			{
				$this->response->statusCode(400);
				$responseData['success'] = false;
				$responseData['message'] = "Could not fetch Farmer data";	
				return json_encode($responseData);
			}
	    } 
	    else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Farmer data";	
			return json_encode($responseData);
		}  				    
	}

	public function get_all_farmers()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);	
	    $upload_path = 'img/';					
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $this->loadModel("Farmer");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    	    	
    	$count = $this->Farmer->find('count');
    	if($offset > 0 )
    	{
    		$offset = $limit * $offset;
    	}
    	    		
    	$my_farmers = $this->Farmer->find('all', array(
    		'fields' => array('Farmer.*'),						          
			'limit' => $limit,
			'offset' => $offset
        	));
    	$my_farmers = Set::classicExtract($my_farmers, '{n}.Farmer');
    	
    	if($my_farmers)
    	{
    		$this->response->statusCode(200);
	    	$responseData['message'] = "Farmer list Successfully fetched";
	    	$responseData['success'] = true;
	    	$responseData['data'] = $my_farmers;
	    	$responseData['offset'] = $offset;
	    	$responseData['count'] = $count;
	    	$responseData = array_map(function($v){
		        return (is_null($v)) ? "" : $v;
		    },$responseData);
			return json_encode($responseData);
		}
		else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Farmer data";	
			return json_encode($responseData);
		}	    		   
	}

	public function save_product()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $decoded = $_POST;
	    $this->loadModel("Product");	
	    $this->loadModel("Product_Speciality");	
	    $this->loadModel("Product_Benefits");	
	    $this->loadModel("Product_Feature");	
	    $this->loadModel("Product_Specifications");	
	    //$decoded['product_benefits'][$i] = array('product_benefits' => $filename[$i]);				
	    
	    if(!empty($decoded))
	    {
	    	if(isset($decoded['product_benefits']) && sizeof($decoded['product_benefits'])>0)
	    	{
	    		foreach ($decoded['product_benefits'] as $key => &$value) {
	    			$decoded['product_benefits'][$key] = array('product_benefits' => $value);				
	    		}
	    	}
	    	if(isset($decoded['product_specialities']) && sizeof($decoded['product_specialities'])>0)
	    	{
	    		foreach ($decoded['product_specialities'] as $key => &$value) {
	    			$decoded['product_specialities'][$key] = array('product_speciality' => $value);				
	    		}
	    	}
	    	if(isset($decoded['product_features']) && sizeof($decoded['product_features'])>0)
	    	{
	    		foreach ($decoded['product_features'] as $key => &$value) {
	    			$decoded['product_features'][$key] = array('product_feature' => $value);				
	    		}
	    	}

	    	if(isset($decoded['product_specifications']) && sizeof($decoded['product_specifications'])>0)
	    	{	    	    	
	    		foreach ($decoded['product_specifications'] as $key => &$value) {		
	    			$decoded['product_specifications'][$key] = array(
	    				'parameter' => $value['parameter'],
	    				'specifications' => $value['specifications']
	    			);				
	    		}
	    	}

			if(isset($_FILES['product_picture'] ) && count($_FILES['product_picture']) > 0) 
		    {
				$num_files = count($_FILES['product_picture']);
			}
		    else
		    	$num_files = 0;
		    if($num_files>0)
			{
				$filename = $_FILES['product_picture']['name'];	
				$url = Router::url('/',true);
				$upload_path = 'img/';				
				$upload_file = $upload_path.$filename;
				if(move_uploaded_file($_FILES['product_picture']['tmp_name'], $upload_file))
				{
					$decoded['product_picture'] = $filename;				
				}
				else
				{
					$decoded['product_picture'] = '';	
				}
			}
	    	$product_data = array(
			    'Product' => array(
			    	'product_name' => $decoded['product_name'],
			    	'price' => $decoded['price'],
			    	'product_picture' => $decoded['product_picture'],
			    	'product_description' => $decoded['product_description'],
			    	'product_application' => $decoded['product_application'],
			    	'cat_id' => $decoded['cat_id'],
					'packing_variants' => $decoded['packing_variants'],
			    	'dosage' => $decoded['dosage']
			    ),
			    'Product_Benefits' =>  $decoded['product_benefits'],
			    'Product_Speciality' =>  $decoded['product_specialities'],
			    'Product_Feature' =>  $decoded['product_features'],
			    'Product_Specifications' =>  $decoded['product_specifications']
			);
			$check = $this->Product->saveAssociated($product_data, array('deep' => true));
			$lastInsertId = $this->Product->getLastInsertId();
			if($check)
			{
				$this->response->statusCode(200);
				$responseData['success'] = true;
				$responseData['message'] = "Product Saved Successfully";	
				return json_encode($responseData);
			}
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Product Not Saved";	
			return json_encode($responseData);
	    }
	    $this->response->statusCode(400);
		$responseData['success'] = false;
		$responseData['message'] = "Product Not Added";	
		return json_encode($responseData);
	}

	public function get_all_product_categories()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
	    $upload_path = 'img/';				
	    $this->loadModel("Reporters");	
	    $this->loadModel("Report");	 
	    $this->loadModel("Product");	
	    $this->loadModel("Product_Category");	
	    $this->loadModel("Product_Speciality");	
	    $this->loadModel("Product_Benefits");	 
	    $limit = 1;
	    $offset = 0;
		if(isset($decoded['limit']))
		{
			$limit = $decoded['limit'];
		}
		if(isset($decoded['offset']))
		{
			$offset = $decoded['offset'];
		}		
	    	    	
    	$count = $this->Product_Category->find('count');
    	if($offset > 0 )
    	{
    		$offset = $limit * $offset;
    	}
    	    		
    	$my_products = $this->Product_Category->find('all', array(
    		'recursive' => 2,					          
			'limit' => $limit,
			'offset' => $offset
        	));
			
    	if($my_products)
    	{    		
    		foreach ($my_products as $key => &$value) 
    		{    			
    			foreach ($value['Product'] as $k => &$val) {
    				if(isset($val['product_picture']) && $val['product_picture'] !='')
					{
						$val['product_picture'] = $url.$upload_path.$val['product_picture'];
					}    				
    			}
    			//$value['Product']['product_picture'] = $url.$upload_path.$value['Product']['product_picture'];
    		}
    		$this->response->statusCode(200);
	    	$responseData['message'] = "Product Category list Successfully fetched";
	    	$responseData['success'] = true;
	    	$responseData['data'] = $my_products;
	    	$responseData['offset'] = $offset;
	    	$responseData['count'] = $count;
	    	$responseData = array_map(function($v){
		        return (is_null($v)) ? "" : $v;
		    },$responseData);
			return json_encode($responseData);
		}
		else
		{
			$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Could not fetch Product Category data";	
			return json_encode($responseData);
		}	    		   
	}	

	public function save_product_category()
	{
		$data = file_get_contents("php://input");
	    $decoded = json_decode($data, true);
	    $url = Router::url('/',true);
	    $upload_path = 'img/';	
	    $this->loadModel("Product_Category");		    
	    if(!empty($decoded))
	    {
	    	if(isset($decoded['cat_name']) && $decoded['cat_name'] !='')
	    	{
	    		$check = $this->Product_Category->save($decoded);
				$lastInsertId = $this->Product_Category->getLastInsertId();
				$Category_data = array('category_id' => $lastInsertId);
				if($check)
				{
					$this->response->statusCode(200);
					$responseData['success'] = true;
					$responseData['message'] = "Product Category Saved Successfully";	
					$responseData['data'] = $Category_data;
					return json_encode($responseData);
				}
				$this->response->statusCode(400);
				$responseData['success'] = false;
				$responseData['message'] = "Product Category Not Saved";	
				return json_encode($responseData);
	    	}
	    	$this->response->statusCode(400);
			$responseData['success'] = false;
			$responseData['message'] = "Product Category Not Saved";	
			return json_encode($responseData);
	    }
	    $this->response->statusCode(400);
		$responseData['success'] = false;
		$responseData['message'] = "Product Category Not Saved";	
		return json_encode($responseData);			
	}

	public function about_us()
	{
		$this->loadModel("About_Us");		    
		$about_us = $this->About_Us->find();
		if(!empty($about_us))
		{
			$this->response->statusCode(200);
			$responseData['success'] = true;
			$responseData['message'] = "About Us Data Successfully fetched";	
			$responseData['data'] = $about_us;
			return json_encode($responseData);
		}
		$this->response->statusCode(400);
		$responseData['success'] = false;
		$responseData['message'] = "No About us data present";	
		return json_encode($responseData);		
	}
}//end of controller
?>