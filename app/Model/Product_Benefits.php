<?php
App::uses('AppModel', 'Model');

class Product_Benefits extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'product_benefits';
	
	public $belongsTo = array(
        'Product' => array(
            'counterCache' => true,
        )        
    );
}