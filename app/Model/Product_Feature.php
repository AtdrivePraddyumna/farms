<?php
App::uses('AppModel', 'Model');

class Product_Feature extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'product_features';
	
	public $belongsTo = array(
        'Product' => array(
            'counterCache' => true,
        )
    );
}