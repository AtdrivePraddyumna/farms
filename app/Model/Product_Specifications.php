<?php
App::uses('AppModel', 'Model');

class Product_Specifications extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'product_specifications';
	
	public $belongsTo = array(
        'Product' => array(
            'counterCache' => true,
        )        
    );
}