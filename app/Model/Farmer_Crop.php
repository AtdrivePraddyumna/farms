<?php
App::uses('AppModel', 'Model');

class Farmer_Crop extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'farmer_crops';
	
	public $belongsTo = array(
        'Farmer' => array(
            'counterCache' => true,
        )
    );
}