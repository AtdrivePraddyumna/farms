<?php
App::uses('AppModel', 'Model');

class Report extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'reports';
	
	public $belongsTo = array(
        'Farmer' => array(
            'counterCache' => true,
        )
    );
    public $hasMany = array(
         'Followed_Report' => array(
            'className' => 'Followed_Report',
            'foreignKey' => 'report_id',
   			'dependent'=>true
        )
    );
}