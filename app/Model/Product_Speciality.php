<?php
App::uses('AppModel', 'Model');

class Product_Speciality extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'product_specialities';
	
	public $belongsTo = array(
        'Product' => array(
            'counterCache' => true,
        )
    );
}