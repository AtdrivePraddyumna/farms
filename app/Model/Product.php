<?php
App::uses('AppModel', 'Model');

class Product extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'products';
	
	public $belongsTo = array(
        'Product_Category' => array(
            'counterCache' => true,
        )
    );
	public $hasMany = array(
         'Product_Benefits' => array(
            'className' => 'Product_Benefits',
            'foreignKey' => 'product_id',
   			'dependent'=>true
        ),
         'Product_Speciality' => array(
            'className' => 'Product_Speciality',
            'foreignKey' => 'product_id',
            'dependent'=>true,
            //'cascadeCallbacks' => true,
        ),
         'Product_Feature' => array(
            'className' => 'Product_Feature',
            'foreignKey' => 'product_id',
            'dependent'=>true,
            //'cascadeCallbacks' => true,
        ),
          'Product_Specifications' => array(
            'className' => 'Product_Specifications',
            'foreignKey' => 'product_id',
            'dependent'=>true,
            //'cascadeCallbacks' => true,
        )
        
    );

}