<?php
App::uses('AppModel', 'Model');

class Product_Category extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'product_categories';

	
	public $hasMany = array(
         'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'cat_id',
            'dependent'=>true
        )        
    );
}