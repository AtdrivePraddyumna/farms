<?php
App::uses('AppModel', 'Model');

class Farmer extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'farmers';

	public $hasOne = array(
        'Farmer_Crop' => array(
            'className' => 'Farmer_Crop',
            'foreignKey' => 'farmer_id',
   			'dependent'=>true,
            'cascadeCallbacks' => true,
        ),
        'Report' => array(        	
            'className' => 'Report',
            'foreignKey' => 'farmer_id',
   			'dependent'=>true,
            'cascadeCallbacks' => true,
        	)
    );
	// public $hasMany = array(
 //         'Farmer_Crop' => array(
 //            'className' => 'Farmer_Crop',
 //            'foreignKey' => 'farmer_id',
 //   			'dependent'=>true
 //        ),
        
 //    );
}