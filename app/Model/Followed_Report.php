<?php
App::uses('AppModel', 'Model');

class Followed_Report extends AppModel {

	public $primaryKey = 'id';
	public $useTable = 'followed_reports';
	
	public $belongsTo = array(
        'Report' => array(
            'counterCache' => true,
        )
    );
}